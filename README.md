

# square-limit: "square limit" pictures using Kawa Scheme #

![sq-lim-kawa.png](sq-lim-kawa.png) ![sq-lim-gnu.png](sq-lim-gnu.png)

Copyright 2016-10-30 [Sudarshan S Chawathe](http://chaw.eip10.org/).

License: [GNU GPL v3 ](https://www.gnu.org/licenses/gpl.html)

For details of the "Picture Language", see Section 2.2.4 of
[SICP](https://mitpress.mit.edu/sicp/).

For details of (kawa pictures), see the 
[Kawa manual](https://www.gnu.org/software/kawa/index.html).

The
[GNUhead.png](https://en.wikipedia.org/wiki/File:Heckert_GNU_white.svg)
image may be found on Wikipedia (or replaced with another image of
course).

Invoke as 'kawa square-limit.scm' Output is written to a file in
/tmp (or equivalent) by default.  Network access is needed only to
download a sample base image.  The comments outline a few other
options.

This tiny "project" is just something that happened while I was
experimenting with the (kawa pictures) library. I put it here
mainly because it is a nice, small example of abstraction at work and
because it is fun to play with the pictures. Enjoy!
