;;; square-limit.scm: SICP's "square limit" pictures using (kawa pictures)
;; Copyright 2016-10-30 Sudarshan S Chawathe <chaw@eip10.org>
;; License: GNU GPL v3 https://www.gnu.org/licenses/gpl.html
;;
;; For details of the "Picture Language", see Section 2.2.4 of SICP:
;;   https://mitpress.mit.edu/sicp/
;; For details of (kawa pictures), see the Kawa manual:
;;   https://www.gnu.org/software/kawa/index.html
;; The "GNUhead.png" image may be found on Wikipedia:
;;   https://en.wikipedia.org/wiki/File:Heckert_GNU_white.svg
;; (or replaced with another image of course).
;;
;; Invoke as 'kawa square-limit.scm' Output is written to a file in
;; /tmp (or equivalent) by default.  Network access is needed only to
;; download a sample base image.  The comments outline a few other
;; options.

;;; Set-up

(import (kawa pictures)
        (kawa swing)) ; not needed if picture is only written to file.

;; The base image used to compose the square limit picture:
;; (define base-image (image-read "GNUhead.png"))
;; (define base-image (scale 4 (image-read "kawa-logo.png")))
(define base-image
  (scale 4 (image-read
            "https://www.gnu.org/software/kawa/style/kawa-logo.png")))

;;; Interface between (kawa pictures) and SICP's Picture Language
;; We can generate square-limit pictures without delving into the
;; "painting a picture into a frame" metaphor by using the facilities
;; of (kawa pictures).  However, that metaphor can easily be supported
;; using affine-transform if needed (for more general pictures).

(define (beside left-pic right-pic)
  (let ((dim &D[0.5 1]))
    (re-center 'origin
               (hbox (scale dim left-pic)
                     (scale dim right-pic)))))

(define (below bottom-pic top-pic)
  (let ((dim &D[1 0.5]))
    (re-center 'origin
               (vbox (scale dim top-pic)
                     (scale dim bottom-pic)))))

(define (flip-vert pic)
  (with-transform (affine-transform 1 0 0 -1 0 0)
                  pic))

(define (flip-horiz pic)
  (with-transform (affine-transform -1 0 0 1 0 0)
                  pic))

;;; SICP's Picture Language (subset) essentially verbatim
;; I left the 'painter' names as they are to emphasize preservation of
;; abstraction boundaries, although 'picture' may be a more
;; appropriate name in this implementation.

(define (right-split painter n)
  (if (zero? n)
      painter
      (let ((smaller (right-split painter (- n 1))))
        (beside painter (below smaller smaller)))))

(define (up-split painter n)
  (if (zero? n)
      painter
      (let ((smaller (up-split painter (- n 1))))
        (below painter (beside smaller smaller)))))

(define (corner-split painter n)
  (if (zero? n)
      painter
      (let ((up (up-split painter (- n 1)))
            (right (right-split painter (- n 1))))
        (let ((top-left (beside up up))
              (bottom-right (below right right))
              (corner (corner-split painter (- n 1))))
          (beside (below painter top-left)
                  (below bottom-right corner))))))

(define (square-limit painter n)
  (let ((quarter (corner-split painter n)))
    (let ((half (beside (flip-horiz quarter) quarter)))
      (below (flip-vert half) half))))

;;; Main actions

;; Larger values of the second argument to square-limit will need a
;; larger (or scaled up) base-image for all the detais to be visible.
(let ((pic (square-limit base-image 3)))
  ;; Java Swing display:
  ;; (show-picture pic)
  ;; SVG output (can get very large for a large base-image):
  ;; (let ((f (make-temporary-file "square-limit-~d.svg")))
  ;;   (picture-write-svg pic f)
  ;;   (display f)
  ;;   (newline))
  ;; PNG output:
  (let ((f (make-temporary-file "square-limit-~d.png")))
    (image-write pic f)
    (display f)
    (newline))
  )

;;; End
